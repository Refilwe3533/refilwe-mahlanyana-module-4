import 'package:app_module_3/edit_profile_view.dart';
import 'package:flutter/material.dart';

class DashboardView extends StatefulWidget {
  const DashboardView({Key? key}) : super(key: key);

  @override
  _DashboardViewState createState() => _DashboardViewState();
}

class _DashboardViewState extends State<DashboardView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.favorite),
        onPressed: () {

        },
      ),
      appBar: AppBar(
        title:const Text("Dashboard"),),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Card(
                elevation: 2,
                shadowColor: Colors.pink,
                child:Column(
                  children: [
                    const Text("Feature 1",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20),),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Image.network("https://media.istockphoto.com/photos/tree-on-grassy-field-against-cloudy-sky-picture-id649725910?k=20&m=649725910&s=612x612&w=0&h=K93SOy6sHHqIWmmXJXBQz8_sRSgZn8XAMogXE8zGKzo=",),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ConstrainedBox(
                      constraints: const BoxConstraints.tightFor(width: 250, height: 60),
                      child: ElevatedButton(
                          onPressed: (){
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => const DashboardView()),
                            );
                          },
                          child: const Text("Feature 1",style: TextStyle(color: Colors.white,fontSize: 17.0,fontWeight: FontWeight.bold),),
                          style: ElevatedButton.styleFrom(shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)))
                      ),
              ),
                    ),
                  ],
                ) ,),
              
              Card(
                elevation: 2,
                shadowColor: Colors.pink,
                child: Column(
                  children: [
                    const Text("Feature 2",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20),),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Image.network("https://media.istockphoto.com/photos/hot-air-balloons-flying-over-the-valley-at-cappadocia-turkey-picture-id926425076?b=1&k=20&m=926425076&s=170667a&w=0&h=nMgJDNCJql8yCNDgTggJJazUfAR_1rRAhe-YzUSy7zw=",),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ConstrainedBox(
                        constraints: const BoxConstraints.tightFor(width: 250, height: 60),
                        child: ElevatedButton(
                            onPressed: (){
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => const DashboardView()),
                              );
                            },
                            child: const Text("Feature 2",style: TextStyle(color: Colors.white,fontSize: 17.0,fontWeight: FontWeight.bold),),
                            style: ElevatedButton.styleFrom(shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)))
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Card(
                elevation: 2,
                shadowColor: Colors.pink,
                child: Column(
                  children: [
                    const Text("Edit Profile",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20),),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Image.network("https://picturecorrect-wpengine.netdna-ssl.com/wp-content/uploads/2016/11/landscape-photography-components.jpg",),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ConstrainedBox(
                        constraints: const BoxConstraints.tightFor(width: 250, height: 60),
                        child: ElevatedButton(
                            onPressed: (){
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => const ProfileView()),
                              );
                            },
                            child: const Text("Edit Profile",style: TextStyle(color: Colors.white,fontSize: 17.0,fontWeight: FontWeight.bold),),
                            style: ElevatedButton.styleFrom(shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)))
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

