import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:app_module_3/login_view.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Dramatique',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          floatingActionButtonTheme: const FloatingActionButtonThemeData(
            backgroundColor: Colors.pinkAccent,
          ),
          primarySwatch: Colors.pink,
            ),
        home: AnimatedSplashScreen(
            duration: 3000,
            splash:Lottie.asset("assets/pride.json"),
            nextScreen: LoginView(),
            splashTransition: SplashTransition.fadeTransition,
            backgroundColor: Colors.white));
  }
}

